all : composer start
test : start phpcs phpmd phpunit-code-coverage-text stop
.PHONY: all start stop build composer phpunit phpunit-code-coverage-html phpunit-code-coverage-text phpcs phpmd

start :
	docker-compose up -d

stop :
	docker-compose down

build :
	cd docker && ./build.sh && cd ..

composer :
	docker-compose run --rm --no-deps php composer install

composer-dump-autoload :
	docker-compose run --rm --no-deps php composer dump-autoload

phpunit :
	docker-compose exec php sh -c "./vendor/bin/phpunit"

phpunit-code-coverage-html :
	docker-compose exec php sh -c "./vendor/bin/phpunit --coverage-html coverage/"

phpunit-code-coverage-text :
	docker-compose exec php sh -c "./vendor/bin/phpunit --coverage-text"

phpcs :
	docker-compose exec php sh -c "./vendor/bin/phpcs"

phpmd :
	docker-compose exec php sh -c "./vendor/bin/phpmd ./src,./tests text phpmd.xml.dist"
