<?php

namespace Commocore\Petscii\Browser;

class SingularBrowser extends AbstractBrowser implements Browseable, PetsciiBrowseable
{
    /**
     * @return array
     */
    public function getUserAgentKeywords()
    {
        return array(
            'singular'
        );
    }
}
