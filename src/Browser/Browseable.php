<?php

namespace Commocore\Petscii\Browser;

interface Browseable
{
    /**
     * @param string $userAgent
     * @return bool
     */
    public function doesUserAgentMatch($userAgent);

    /**
     * @return array
     */
    public function getUserAgentKeywords();

    /**
     * @return array
     */
    public function getTransliterations();
}
