<?php

namespace Commocore\Petscii\Browser;

class BrowserFactory
{
    /**
     * @var Browseable[]
     */
    private $registeredBrowsers = array();

    /**
     * @param Browseable[]|null $userDefinedBrowsers
     */
    public function __construct(array $userDefinedBrowsers = null)
    {
        $this->registerSupportedBrowsers();
        if (!empty($userDefinedBrowsers)) {
            $this->registerUserDefinedBrowsers($userDefinedBrowsers);
        }
        $this->registerFallbackBrowser();
    }

    /**
     * @return Browseable[]
     */
    public function getRegisteredBrowsers()
    {
        return $this->registeredBrowsers;
    }

    /**
     * @return Browseable[]
     */
    private function getSupportedBrowsers()
    {
        return array(
            new ContikiBrowser(),
            new SingularBrowser(),
            new HyperLinkBrowser()
        );
    }

    /**
     * @param Browseable $browser
     * @return void
     */
    private function registerBrowser(Browseable $browser)
    {
        array_push(
            $this->registeredBrowsers,
            $browser
        );
    }

    /**
     * @return void
     */
    private function registerSupportedBrowsers()
    {
        foreach ($this->getSupportedBrowsers() as $browser) {
            $this->registerBrowser($browser);
        }
    }

    /**
     * @param Browseable[] $userDefinedBrowsers
     * @return void
     */
    private function registerUserDefinedBrowsers(array $userDefinedBrowsers)
    {
        foreach ($userDefinedBrowsers as $browser) {
            $this->registerBrowser($browser);
        }
    }

    /**
     * @return void
     */
    private function registerFallbackBrowser()
    {
        $this->registerBrowser(new GenericBrowser());
    }
}
