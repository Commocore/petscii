<?php

namespace Commocore\Petscii\Browser;

class HyperLinkBrowser extends AbstractBrowser implements Browseable, PetsciiBrowseable
{
    /**
     * @return array
     */
    public function getUserAgentKeywords()
    {
        return array(
            'hyperlink'
        );
    }
}
