<?php

namespace Commocore\Petscii\Browser;

use Commocore\Petscii\Transliteration\PoundSterling;

class ContikiBrowser extends AbstractBrowser implements Browseable, PetsciiBrowseable
{
    /**
     * @return array
     */
    public function getUserAgentKeywords()
    {
        return array(
            'contiki'
        );
    }

    /**
     * @return array
     */
    public function getTransliterations()
    {
        return array(
            new PoundSterling()
        );
    }
}
