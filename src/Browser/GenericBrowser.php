<?php

namespace Commocore\Petscii\Browser;

class GenericBrowser extends AbstractBrowser implements Browseable, PetsciiBrowseable
{
    /**
     * Possible keywords to be expected for HTTP User-Agent which might determine a Petscii type browser
     *
     * @return array
     */
    public function getUserAgentKeywords()
    {
        return array(
            'commodore', 'c64', 'petscii'
        );
    }
}
