<?php

namespace Commocore\Petscii\Browser;

abstract class AbstractBrowser implements Browseable
{
    /**
     * @param string $userAgent
     * @return bool
     */
    public function doesUserAgentMatch($userAgent)
    {
        foreach ($this->getUserAgentKeywords() as $keyword) {
            if (stristr($userAgent, $keyword)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getUserAgentKeywords()
    {
        return array();
    }

    /**
     * @return array
     */
    public function getTransliterations()
    {
        return array();
    }
}
