<?php

namespace Commocore\Petscii\Transliteration;

interface Transliterable
{
    public function fromCharacter();

    public function toCharacter();

    public function getExchanger();
}
