<?php

namespace Commocore\Petscii\Transliteration;

/**
 * Pound Sterling exchanged used for Contiki browser
 */
class PoundSterling implements Transliterable
{
    const EXCHANGER = '//POUND_STERLING//';

    /**
     * @return array
     */
    public function fromCharacter()
    {
        return array(
            '&#163;',
            '£'
        );
    }

    /**
     * @return string
     */
    public function toCharacter()
    {
        return chr(92);
    }

    /**
     * @return string
     */
    public function getExchanger()
    {
        return self::EXCHANGER;
    }
}
