<?php

namespace Commocore\Petscii\Tag;

/**
 * Replace all HTML break lines variations to <br>
 * e.g. FairlightML offline browser cannot detect other variations
 */
class BreakLine implements TagConvertible
{
    const REPLACE_WITH = '<br>';

    /**
     * @param string $text
     * @return string
     */
    public function convert($text)
    {
        return preg_replace('/<br\s*\/?>/i', self::REPLACE_WITH, $text);
    }
}
