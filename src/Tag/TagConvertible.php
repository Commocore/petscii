<?php

namespace Commocore\Petscii\Tag;

interface TagConvertible
{
    public function convert($text);
}
