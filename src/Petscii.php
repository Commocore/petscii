<?php

namespace Commocore\Petscii;

use Behat\Transliterator\Transliterator;
use InvalidArgumentException;
use Commocore\Petscii\Transliteration\Transliterable;
use Commocore\Petscii\Tag\TagConvertible;
use Commocore\Petscii\Tag\BreakLine;
use Commocore\Petscii\Browser\Browseable;
use Commocore\Petscii\Browser\PetsciiBrowseable;
use Commocore\Petscii\Browser\BrowserFactory;
use Commocore\Petscii\Browser\NonPetsciiBrowser;

class Petscii
{
    /**
     * @var BrowserFactory
     */
    private $browserFactory;

    /**
     * @var Browseable
     */
    private $browser;

    /**
     * @var array
     */
    private $forthTableFrom = array();

    /**
     * @var array
     */
    private $forthTableTo = array();

    /**
     * @var array
     */
    private $backTableTo = array();

    /**
     * @param Browseable[]|null $userDefinedBrowsers
     */
    public function __construct(array $userDefinedBrowsers = null)
    {
        $this->browserFactory = new BrowserFactory($userDefinedBrowsers);

        $this->browser = $this->getDetectedBrowser();
        $this->buildTransliterationTable($this->browser);
    }

    /**
     * Matching browser by HTTP user agent
     * Note: filter_input function would return null on some implementations of FCGI/PHP 5.4
     * (and probably older versions as well) so super global $_SERVER has been accessed here directly
     *
     * @return Browseable
     */
    public function getDetectedBrowser()
    {
        if (!isset($_SERVER['HTTP_USER_AGENT'])) {
            return new NonPetsciiBrowser();
        }

        $userAgent = $_SERVER["HTTP_USER_AGENT"];
        $browsers = $this->browserFactory->getRegisteredBrowsers();

        foreach ($browsers as $browser) {
            if ($browser->doesUserAgentMatch($userAgent)) {
                return $browser;
            }
        }

        return new NonPetsciiBrowser();
    }

    /**
     * @return bool
     */
    public function isPetsciiBrowser()
    {
        return $this->browser instanceof PetsciiBrowseable;
    }

    /**
     * Returns string in PETSCII mode
     *
     * @param string $text
     * @param string|null $trimMask, i.e. <br>
     * @throws InvalidArgumentException
     * @return string
     */
    public function render($text, $trimMask = null)
    {
        if (!is_string($text)) {
            return '';
        }

        if (!is_null($trimMask) && !is_string($trimMask)) {
            throw new InvalidArgumentException('Trim mask value must be a string, or null');
        }

        if (!$this->isPetsciiBrowser()) {
            $text = $this->convertTags($text);

            return trim($text, $trimMask);
        }

        $text = $this->convertTags($text);
        $text = $this->transliterateForth($text);
        $text = $this->transliterateToAscii($text, $trimMask);
        $text = $this->transliterateBack($text);

        return $text;
    }

    /**
     * @param string $text
     * @return string
     */
    public static function trimHtmlLineBreaks($text)
    {
        $text = preg_replace('/^(?:\s*<br *\/?>\s*)+/i', '', $text);
        $text = preg_replace('/(?:\s*<br *\/?>\s*)+$/i', '', $text);

        return $text;
    }

    /**
     * @return string
     */
    public function getTestPage()
    {
        $content = '';
        for ($i=0; $i<=255; $i++) {
            $content .= $i . ': ' . chr($i) . '<br>' . PHP_EOL;
        }

        return $content;
    }

    /**
     * @param Browseable $browser
     */
    private function buildTransliterationTable(Browseable $browser)
    {
        $forthTableFrom = array();
        $forthTableTo = array();
        $backTableTo = array();
        $transliterations = $browser->getTransliterations();

        if (empty($transliterations)) {

            return;
        }

        foreach ($transliterations as $transliteration) {
            /** @var Transliterable $transliteration */
            if (in_array($transliteration->getExchanger(), $forthTableTo)) {
                throw new InvalidArgumentException(
                    'Exchanger "' . $transliteration->getExchanger() . '" already in use'
                );
            }
            foreach ($transliteration->fromCharacter() as $from) {
                $forthTableFrom[] = $from;
                $forthTableTo[] = $transliteration->getExchanger();
            }
            $backTableTo[$transliteration->getExchanger()] = $transliteration->toCharacter();
        }
        $this->setForthTableFrom($forthTableFrom);
        $this->setForthTableTo($forthTableTo);
        $this->setBackTableTo($backTableTo);
    }

    /**
     * @param array $table
     */
    private function setForthTableFrom(array $table)
    {
        $this->forthTableFrom = $table;
    }

    /**
     * @param array $table
     */
    private function setForthTableTo(array $table)
    {
        $this->forthTableTo = $table;
    }

    /**
     * @param array $table
     */
    private function setBackTableTo(array $table)
    {
        $this->backTableTo = $table;
    }

    /**
     * @param string $input
     * @return string
     */
    private function transliterateForth($input)
    {
        return str_replace(
            array_values($this->forthTableFrom),
            array_values($this->forthTableTo),
            $input
        );
    }

    /**
     * @param string $text
     * @param string|null $trimMask
     * @return string
     */
    private function transliterateToAscii($text, $trimMask = null)
    {
        return trim(Transliterator::utf8ToAscii($text), $trimMask);
    }

    /**
     * @param string $input
     * @return string
     */
    private function transliterateBack($input)
    {
        return str_replace(
            array_values(array_flip($this->backTableTo)),
            array_values($this->backTableTo),
            $input
        );
    }

    /**
     * @param string $text
     * @return string
     */
    private function convertTags($text)
    {
        foreach ($this->getTags() as $tag) {
            $text = $tag->convert($text);
        }

        return $text;
    }

    /**
     * @return TagConvertible[]
     */
    private function getTags()
    {
        return array(
            new BreakLine()
        );
    }
}
