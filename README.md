About Petscii
=============

Petscii is a tiny PHP library that enables text rendering in the PETSCII format. It allows browsers on the original 8-bit Commodore machines (or emulators) to properly display your website.

- It converts any text to the PETSCII format (PET Standard Code of Information Interchange, a character set based on ASCII).
- It detects PETSCII browsers, allowing for website layout customization for such browsers.

This package is used on the [www.commocore.com](https://www.commocore.com) website.


Conversions
===========

- All non-ASCII characters are converted to basic ASCII-96.
- All variations of the break line tag (e.g. `<br />`) are converted to `<br>`.
- In _Contiki_, the Pound Sterling character is converted to the corresponding PETSCII character `CHR$(92)`.


Supported browsers
==================

Note: The PETSCII format is used in Commodore Business Machines - the 8-bit home computers such as VIC-20, C64, C128, CBM-II, Commodore Plus/4, C16 and C116. 
Ethernet cartridges, such as _64NIC+_, _The Final Ethernet_, _RR-Net_ can be used to connect to the internet. 

The Petscii package has been tested on the Commodore 64 browsers listed below. 
It is possible to add user-defined browsers to Petscii. 

Contiki web browser
-------------------
An online HTML web browser available with a web server and other tools.
Set up disk configurator for version 2.5 available [here](http://contiki.cbm8bit.com).
_Contiki 3_ is available [here](http://retrohackers.com/viewtopic.php?f=5&t=669).

Singular browser
----------------
Online HTML web browser. About the browser [here](http://singularcrew.hu/browser/). 
Set up disk available [here](http://home.sch.bme.hu/~soci64/browser/compile.php).

HyperLink
---------
Online HTML web browser. Not tested yet, but detected by PETSCII. More info [here](http://www.armory.com/~spectre/cwi/hl/manual/index.html).

FairlightML
-----------
Offline HTML viewer, also known as _64'er htmlreader_. Version 0.99 is available
[here](http://commodore.software/downloads/download/151-miscellaneous-applications/1050-fairlightml-v0-99.html).


Entering the World Wide Web via an Emulator
============================================

You can try using [Vice64 emulator](http://vice-emu.sourceforge.net) for Windows (with
[WinPcap](http://winpcap.org/install/default.htm) installed). You will find more details in this
[Commodore Server blog entry](http://www.commodoreserver.com/BlogEntryView.asp?EID=CDB68A9028654CCEA758D37C6DB3E05B).


Installation
============

To install via Composer:

    composer require commocore/petscii

To autoload via Composer (for more details, see Composer documentation):
 
    require_once '../vendor/autoload.php';

To import Petscii to your code:

    use Commocore\Petscii\Petscii;


Usage
=====

Petscii will convert input text only when a PETSCII browser is detected (based on HTTP User Agent).
Otherwise, it will display content without any changes.

    $content = 'Commodore 64<br />The Commodore 64 is an 8-bit home computer introduced in January 1982.<br />';
    
    $petscii = new Petscii();
    
    echo $petscii->render($content);


To trim single characters from the beginning and the end of a string, provide the trim mask string as the second
parameter (PHP's `trim()` mask is used):

    $content = $petscii->render($content, "\n");


To control blank line spacing, you can also trim HTML line breaks (`<br>`) from the beginning and the end of the string using:

    $result = Petscii::trimHtmlLineBreaks($this->petscii->render($text));


To check if a browser supports PETSCII:

    if ($petscii->isPetsciiBrowser()) {
        // I'm PETSCII!
    }


To return browser class detected by HTTP User Agent:

    $browserClass = $petscii->getDetectedBrowser();
    
Or, to get only the class name (without a namespace):

    $className = substr(strrchr(get_class($petscii->getDetectedBrowser()), "\\"), 1);


To add another browser, implement the `Browseable` and `PetsciiBrowseable`
interfaces to define a new class. Then, pass it to the constructor in an array, e.g.

    $petscii = new Petscii(
        array(
            new CosmicBrowser()
        )
    );


Testing the package
===================

Docker (with Docker Compose) was used for testing purposes.

Setup
-----

1. Build PHP Docker image by executing `make build`.
2. Execute `make all` in the main directory for the first time to install PHP dependencies and
run containers, or use `make composer`, then `make start`.

Executing tests
---------------

_Makefile_ contains all available _make_ commands. 
To test the whole project: `make test`.
If `make phpunit-code-coverage-html` is used, results will be saved to `coverage/` folder in the HTML format.

Testing available characters
----------------------------

To see characters available in a particular browser, create a test page to generate a list of all 256 characters the following way:

    echo $petscii->getTestPage();


Notes
=====

Underscore character compatibility
----------------------------------

The _Contiki_ browser does not use the underscore character. Left arrow `CHR$(95)` is displayed instead.
You can either allow left arrow characters, or add own code to remove the underscores from your text for _Contiki_.
The underscore displays correctly in other browsers such as _FairlightML_ HTML viewer, or _Singular browser_.

What if my website is served over HTTPS?
----------------------------------------

Unfortunately, due to technical limitations, 8-bit web browsers don't support HTTPS connections.
Most often, _nginx_ or _Apache_ rewrite rules force HTTP connections to redirect to HTTPS.
A quick & dirty solution to the problem is to add an exception to these rules.
Since there are only a few 8-bit web browsers, we can easily create a blacklist to exclude them from the HTTPS rewrites.


Redirect example for _Apache_:

    RewriteEngine On
    RewriteCond %{HTTPS} !=on
    RewriteCond %{HTTP_USER_AGENT} !(Contiki|Singular|Hyperlink) [NC]
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]


Redirect example for _nginx_:

    http {
      map $http_user_agent $petscii_browser {
        default 0;
        ~(Contiki|Singular|Hyperlink) 1;
      }
    
      server {
        if ($petscii_browser = 0) {
          rewrite ^(.*) https://$host$1 permanent;
        }
      }
    }

This way, the HTTPS rewrite won't happen for a particular `User-Agent` if it contains strings such as `Contiki`, `Singular` or `Hyperlink`.
Note that the strings set should contain an up-to-date list of supported browsers.


Get in touch
=============

Please, report any issues with Petscii with [issue tracker](https://bitbucket.org/Commocore/petscii/issues).

Contact me at contact[one happy monkey]commocore.com if:

- you have more questions about Petscii
- you use another (non-listed) browser 
- you noticed that a link in this document no longer works

Thank you!
