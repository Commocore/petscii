# Changelog

## [1.2.1] - 2020-04-01
### Changed
- Edits to README


## [1.2.0] - 2019-10-04
### Added
- Proper trimming of HTML line breaks by introducing `trimHtmlLineBreaks` static method


## [1.1.0] - 2019-10-02
### Added
- Ability to add User defined browsers
- Note about handling HTTP for PETSCII browsers on HTTPS force rewrites

### Changed
- BrowserFactory refactor
- Move tests to different namespace

### Removed
- Obsolete nginx setup from docker-compose


## [1.0.1] - 2018-09-24
### Added
- Make command to build the Docker containers
- This CHANGELOG file

### Removed
- Unused nginx image and creation of the container


## [1.0.0] - 2017-01-28
### Added
- HyperLink browser support
- Testing page generator
- LICENSE file


## [0.4.0] - 2017-01-15
### Added
- Global Make command to run all tests

### Removed
- composer.lock


## [0.3.0] - 2017-01-15
### Added
- PSR-4 autoload
- Browser detection

### Changed
- Location of files

## [0.2.2] - 2016-07-26
### Added
- Newest version of Contiki 3.x to supported browsers

### Changed
- Trim mask for break lines


## [0.2.1] - 2016-07-21
### Changed
- Revert namespace for the package


## [0.2.0] - 2016-07-20
### Added
- HTTP User Agent check
- Make commands
- Additional tests for transliteration and trimming
- Docker with Docker Compose
- PHP Code Sniffer
- PHP Mess Detector
- 100% code coverage (thanks to Phake)

### Changed
- Refactor tests


## [0.1.1] - 2016-06-11
### Changed
- README file


## [0.1.0], [init] - 2016-06-11
### Added
- Initial commit with Pound Sterling character mapping functionality
- README file

### Changed
- Move main class to `src` subfolder


[1.2.1]: https://bitbucket.org/Commocore/petscii/branches/compare/HEAD..v1.2.0#diff
[1.2.0]: https://bitbucket.org/Commocore/petscii/branches/compare/v1.2.0..v1.1.0#diff
[1.1.0]: https://bitbucket.org/Commocore/petscii/branches/compare/v1.1.0..v1.0.1#diff
[1.0.1]: https://bitbucket.org/Commocore/petscii/branches/compare/v1.0.1..v1.0.0#diff
[1.0.0]: https://bitbucket.org/Commocore/petscii/branches/compare/e92c80b308ca0fdc9e844556bacff37bfa534681..2f94fac6326dd3dc2ff36066b03fbf6053ab1af3#diff
[0.4.0]: https://bitbucket.org/Commocore/petscii/branches/compare/2f94fac6326dd3dc2ff36066b03fbf6053ab1af3..9dd5f01d2c9c99b314555edfc1a2c5cd86ea52f7#diff
[0.3.0]: https://bitbucket.org/Commocore/petscii/branches/compare/9dd5f01d2c9c99b314555edfc1a2c5cd86ea52f7..4329553218bf201ab10db63213ab83bb03aae555#diff
[0.2.2]: https://bitbucket.org/Commocore/petscii/branches/compare/4329553218bf201ab10db63213ab83bb03aae555..ff66854e473cda02f01c86f40a43e897168ccf54#diff
[0.2.1]: https://bitbucket.org/Commocore/petscii/branches/compare/ff66854e473cda02f01c86f40a43e897168ccf54..36c5ef843760d7e2b662006f9603da9fb68548d8#diff
[0.2.0]: https://bitbucket.org/Commocore/petscii/branches/compare/36c5ef843760d7e2b662006f9603da9fb68548d8..52562ee0ecb8ff81172f8ddf1052ee8a0fcfe421#diff
[0.1.1]: https://bitbucket.org/Commocore/petscii/branches/compare/52562ee0ecb8ff81172f8ddf1052ee8a0fcfe421..a22015541ba84477ee51d43067f146696c34b19b#diff
[0.1.0]: https://bitbucket.org/Commocore/petscii/branches/compare/a22015541ba84477ee51d43067f146696c34b19b..2614a4de6adeaf8a87a811d56556e0d02bb2c02d#diff
[init]: https://bitbucket.org/Commocore/petscii/commits/2614a4de6adeaf8a87a811d56556e0d02bb2c02d
