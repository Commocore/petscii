<?php

namespace Commocore\Petscii\Tests\UserDefinedBrowser;

use Commocore\Petscii\Browser\AbstractBrowser;
use Commocore\Petscii\Browser\Browseable;
use Commocore\Petscii\Browser\PetsciiBrowseable;
use Commocore\Petscii\Tests\UserDefinedTransliteration\Star;

class CosmicBrowser extends AbstractBrowser implements Browseable, PetsciiBrowseable
{
    /**
     * @return array
     */
    public function getUserAgentKeywords()
    {
        return array(
            'CosmicBrowser'
        );
    }

    /**
     * @return array
     */
    public function getTransliterations()
    {
        return array(
            new Star()
        );
    }
}
