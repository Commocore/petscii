<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Commocore\Petscii\Petscii;

class UserAgentTest extends PHPUnit_Framework_TestCase
{
    public function testEmptyUserAgent()
    {
        unset($_SERVER['HTTP_USER_AGENT']);
        $petscii = new Petscii();
        $this->assertFalse($petscii->isPetsciiBrowser());
    }

    /**
     * @dataProvider userAgentData
     * @param string $httpUserAgent
     * @param string $expectedClassName
     * @param bool $expectedIsPetscii
     */
    public function testSpecificUserAgent($httpUserAgent, $expectedClassName, $expectedIsPetscii)
    {
        $_SERVER['HTTP_USER_AGENT'] = $httpUserAgent;
        $petscii = new Petscii();
        $this->assertInstanceOf($expectedClassName, $petscii->getDetectedBrowser());
        $this->assertEquals($expectedIsPetscii, $petscii->isPetsciiBrowser());
    }

    /**
     * @return array
     */
    public function userAgentData()
    {
        return array(
            array(
                'Contiki/2.2.2 (; http://www.sics.se/contiki/)',
                'Commocore\Petscii\Browser\ContikiBrowser',
                true
            ),
            array(
                'Contiki/1.1-rc0 (Commodore 64; http://dunkels.com/adam/contiki/)',
                'Commocore\Petscii\Browser\ContikiBrowser',
                true
            ),
            array(
                'Contiki/3.x (; http://www.contiki-os.org/)',
                'Commocore\Petscii\Browser\ContikiBrowser',
                true
            ),
            array(
                'Singular/0.52 (Commodore 64)',
                'Commocore\Petscii\Browser\SingularBrowser',
                true
            ),
            array(
                'Lynx (compatible; HyperLink-Parsing-Proxy C64/128)',
                'Commocore\Petscii\Browser\HyperLinkBrowser',
                true
            ),
            array(
                'Mozarella Waterdog browser/5.0',
                'Commocore\Petscii\Browser\NonPetsciiBrowser',
                false
            )
        );
    }
}
