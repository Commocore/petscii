<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Commocore\Petscii\Petscii;

class GenericBrowserTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Petscii
     */
    private $petscii;

    public function setUp()
    {
        $_SERVER['HTTP_USER_AGENT'] = 'I am C64';
        $this->petscii = new Petscii();
    }

    /**
     * @dataProvider trimTextData
     * @param string $text
     * @param string $trimMask
     */
    public function testTrimMask($text, $trimMask)
    {
        $expectedContent = 'The Commodore 64 is an 8-bit home computer introduced in January 1982.';

        $result = $this->petscii->render($text, $trimMask);
        $this->assertEquals($expectedContent, $result);
    }

    /**
     * @dataProvider citiesData
     * @param string $content
     * @param string $expectedContent
     */
    public function testCharacters($content, $expectedContent)
    {
        $content = $this->petscii->render($content);
        $this->assertEquals($expectedContent, $content);
    }

    /**
     * @return array
     */
    public function trimTextData()
    {
        return array(
            array(
                "\nThe Commodore 64 is an 8-bit home computer introduced in January 1982.\n",
                "\n"
            ),
            array(
                "\n\tThe Commodore 64 is an 8-bit home computer introduced in January 1982.\n",
                "\t\n"
            ),
        );
    }

    /**
     * @return array
     */
    public function citiesData()
    {
        return array(
            array(
                'Köln',
                'Koln'
            ),
            array(
                'Reykjavík',
                'Reykjavik'
            ),
            array(
                'Łódź',
                'Lodz'
            ),
            array(
                'Санкт-Петербург',
                'Sankt-Pietierburgh'
            ),
            array(
                'São Paulo',
                'Sao Paulo'
            ),
            array(
                'Olhão da Restauração',
                'Olhao da Restauracao'
            )
        );
    }
}
