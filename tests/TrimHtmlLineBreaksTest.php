<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Commocore\Petscii\Petscii;

class TrimHtmlLineBreaksTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Petscii
     */
    private $petscii;

    public function setUp()
    {
        $this->petscii = new Petscii();
    }

    /**
     * @dataProvider data
     * @param string $expectedText
     * @param string $inputText
     */
    public function testTrimMask($expectedText, $inputText)
    {
        $result = Petscii::trimHtmlLineBreaks($this->petscii->render($inputText));
        $this->assertEquals($expectedText, $result);
    }

    /**
     * @return array
     */
    public function data()
    {
        $text = 'The Commodore 64 is an 8-bit home computer introduced in January 1982.';

        return array(
            array(
                $text,
                $text
            ),
            array(
                $text,
                sprintf('<br>%s<br>', $text)
            ),
            array(
                $text,
                sprintf('<br/>%s<br/>', $text)
            ),
            array(
                $text,
                sprintf('<br />%s<br />', $text)
            ),
            array(
                $text,
                sprintf('<BR>%s<BR>', $text)
            ),
            array(
                $text,
                sprintf('<BR/>%s<BR/>', $text)
            ),
            array(
                $text,
                sprintf('<BR />%s<BR />', $text)
            ),
            array(
                $text,
                sprintf('<br><br>%s<br><br>', $text)
            ),
            array(
                $text,
                sprintf('<BR><BR>%s<BR><BR>', $text)
            ),
            array(
                $text,
                sprintf('<BR/><BR/>%s<BR/><BR/>', $text)
            ),
            array(
                $text,
                sprintf('<BR /><BR />%s<BR /><BR />', $text)
            ),
            array(
                $text,
                sprintf('<br /><BR><br>%s<BR><BR /><br>', $text)
            ),
            array(
                sprintf('<i>%s</i>', $text),
                sprintf('<br><i>%s</i><br>', $text)
            ),
            array(
                sprintf('<div>%s</div>', $text),
                sprintf('<br><div>%s</div><br>', $text)
            )
        );
    }
}
