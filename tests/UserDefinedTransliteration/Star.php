<?php

namespace Commocore\Petscii\Tests\UserDefinedTransliteration;

use Commocore\Petscii\Transliteration\Transliterable;

class Star implements Transliterable
{
    const EXCHANGER = '//STAR//';

    /**
     * @return array
     */
    public function fromCharacter()
    {
        return array(
            '*',
        );
    }

    /**
     * @return string
     */
    public function toCharacter()
    {
        return 'star';
    }

    /**
     * @return string
     */
    public function getExchanger()
    {
        return self::EXCHANGER;
    }
}
