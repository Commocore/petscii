<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Commocore\Petscii\Petscii;
use Commocore\Petscii\Tests\UserDefinedBrowser\CosmicBrowser;

class UserDefinedBrowserTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Petscii
     */
    private $petscii;

    /**
     * @var CosmicBrowser
     */
    private $cosmicBrowser;

    public function setUp()
    {
        $this->cosmicBrowser = new CosmicBrowser();

        $_SERVER['HTTP_USER_AGENT'] = 'CosmicBrowser 2.64';
        $this->petscii = new Petscii(
            array(
                $this->cosmicBrowser
            )
        );
    }

    public function testDetectedBrowser()
    {
        $this->assertInstanceOf(
            get_class($this->cosmicBrowser),
            $this->petscii->getDetectedBrowser()
        );
    }

    public function testTransliteration()
    {
        $content = $this->petscii->render($this->getContent());
        $this->assertEquals('Milky way, moon, planet, star.', $content);
    }

    /**
     * @return string
     */
    private function getContent()
    {
        return 'Milky way, moon, planet, *.';
    }
}
