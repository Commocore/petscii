<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Commocore\Petscii\Tag\BreakLine;

class TagsTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider breakLinesData
     * @param string $text
     */
    public function testRenderingBreakLines($text)
    {
        $breakLine = new BreakLine();
        $this->assertEquals(
            'Break<br>the<br>line<br>and<br>now<br>break<br>the<br>w<br>o<br>r<br>d!<br>',
            $breakLine->convert($text)
        );
    }

    /**
     * @return array
     */
    public function breakLinesData()
    {
        return array(
            array(
                'Break<br />the<br>line<br >and<br  />now<br  >break<BR>the<BR >w<br>o<bR>r<BR />d!<br>'
            )
        );
    }
}
