<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Commocore\Petscii\Petscii;

class HyperLinkBrowserTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Petscii
     */
    private $petscii;

    public function setUp()
    {
        $_SERVER['HTTP_USER_AGENT'] = 'Lynx (compatible; HyperLink-Parsing-Proxy C64/128)';
        $this->petscii = new Petscii();
    }

    /**
     * @dataProvider trimTextData
     * @param string $text
     * @param string $trimMask
     */
    public function testTrimMask($text, $trimMask)
    {
        $expectedContent = 'The Commodore 64 is an 8-bit home computer introduced in January 1982.';

        $result = $this->petscii->render($text, $trimMask);
        $this->assertEquals($expectedContent, $result);
    }

    /**
     * Contiki supports Pound Sterling character, but HyperLink browser
     * will display backslash for chr(92) so do nothing here
     * Transliterator will return "PS"
     *
     * @dataProvider poundSterlingStringData
     * @param string $content
     */
    public function testNotSupportedPoundSterlingCharacter($content)
    {
        $expectedContent = 'Commodore 64 costs only PS360 now!';

        $content = $this->petscii->render($content);
        $this->assertEquals($expectedContent, $content);
    }

    /**
     * @dataProvider citiesData
     * @param string $content
     * @param string $expectedContent
     */
    public function testCharacters($content, $expectedContent)
    {
        $content = $this->petscii->render($content);
        $this->assertEquals($expectedContent, $content);
    }

    /**
     * @return array
     */
    public function trimTextData()
    {
        return array(
            array(
                "\nThe Commodore 64 is an 8-bit home computer introduced in January 1982.\n",
                "\n"
            ),
            array(
                "\n\tThe Commodore 64 is an 8-bit home computer introduced in January 1982.\n",
                "\t\n"
            ),
        );
    }

    /**
     * @return array
     */
    public function poundSterlingStringData()
    {
        return array(
            array(
                'Commodore 64 costs only £360 now!'
            )
        );
    }

    /**
     * @return array
     */
    public function citiesData()
    {
        return array(
            array(
                'Köln',
                'Koln'
            ),
            array(
                'Reykjavík',
                'Reykjavik'
            ),
            array(
                'Łódź',
                'Lodz'
            ),
            array(
                'Санкт-Петербург',
                'Sankt-Pietierburgh'
            ),
            array(
                'São Paulo',
                'Sao Paulo'
            ),
            array(
                'Olhão da Restauração',
                'Olhao da Restauracao'
            )
        );
    }
}
