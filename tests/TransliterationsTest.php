<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Commocore\Petscii\Petscii;

class TransliterationsTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Petscii
     */
    private $petscii;

    public function setUp()
    {
        $_SERVER['HTTP_USER_AGENT'] = 'Contiki/2.2.2 (; http://www.sics.se/contiki/)';
        $this->petscii = new Petscii();
    }

    /**
     * @dataProvider poundData
     * @param string $text
     */
    public function testRenderingPoundData($text)
    {
        $content = $this->petscii->render($text);
        $this->assertEquals('Price in ' . chr(92), $content);
    }

    /**
     * @return array
     */
    public function poundData()
    {
        return array(
            array(
                'Price in £'
            ),
            array(
                'Price in &#163;'
            )
        );
    }
}
