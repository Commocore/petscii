<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Commocore\Petscii\Petscii;
use Commocore\Petscii\Browser\NonPetsciiBrowser;

class NonPetsciiBrowserTest extends PHPUnit_Framework_TestCase
{
    const HTTP_USER_AGENT = 'Mozarella Waterdog browser/5.0';

    /**
     * @var Petscii
     */
    private $petscii;

    public function setUp()
    {
        $_SERVER['HTTP_USER_AGENT'] = self::HTTP_USER_AGENT;
        $this->petscii = new Petscii();
    }

    public function testDoesUserAgentMatch()
    {
        $browser = new NonPetsciiBrowser();
        $this->assertFalse($browser->doesUserAgentMatch(self::HTTP_USER_AGENT));
    }

    /**
     * @dataProvider trimTextData
     * @param string $text
     * @param string $trimMask
     */
    public function testTrimMask($text, $trimMask)
    {
        $expectedContent = 'The Commodore 64 is an 8-bit home computer introduced in January 1982.';

        $result = $this->petscii->render($text, $trimMask);
        $this->assertEquals($expectedContent, $result);
    }

    /**
     * @return array
     */
    public function trimTextData()
    {
        return array(
            array(
                "\nThe Commodore 64 is an 8-bit home computer introduced in January 1982.\n",
                "\n"
            ),
            array(
                "\n\tThe Commodore 64 is an 8-bit home computer introduced in January 1982.\n",
                "\t\n"
            ),
        );
    }
}
