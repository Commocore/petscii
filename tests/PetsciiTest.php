<?php

namespace Commocore\Petscii\Tests;

use PHPUnit_Framework_TestCase;
use Phake;
use InvalidArgumentException;
use Commocore\Petscii\Petscii;

class PetsciiTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Petscii
     */
    private $petscii;

    public function setUp()
    {
        $_SERVER['HTTP_USER_AGENT'] = null;
        $this->petscii = new Petscii();
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testRenderWithInvalidTrimMask()
    {
        $this->petscii->render('', array());
    }

    public function testRenderNonString()
    {
        $this->assertSame('', $this->petscii->render(null));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testExchangerAlreadyInUse()
    {
        $transliteration1 = Phake::partialMock('Commocore\Petscii\Transliteration\PoundSterling');
        Phake::when($transliteration1)->getExchanger()->thenReturn('//EXCHANGER_NAME//');

        $transliteration2 = Phake::partialMock('Commocore\Petscii\Transliteration\PoundSterling');
        Phake::when($transliteration2)->getExchanger()->thenReturn('//EXCHANGER_NAME//');

        $petsciiMock = Phake::mock('Commocore\Petscii\Petscii', Phake::ifUnstubbed()->thenCallParent());
        $browserMock = Phake::mock('Commocore\Petscii\Browser\GenericBrowser');

        Phake::when($browserMock)->getTransliterations()->thenReturn(
            array($transliteration1, $transliteration2)
        );
        Phake::makeVisible($petsciiMock)->buildTransliterationTable($browserMock);
        $petsciiMock->buildTransliterationTable($browserMock);
    }

    public function testGetTestPage()
    {
        $this->assertTrue(is_string($this->petscii->getTestPage()));
    }
}
